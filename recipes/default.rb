


=begin
 
1) Add embedded mysql
  http://bugzilla.readthedocs.org/en/latest/installing/quick-start.html
=end

service "apache2" do
  supports :restart => true, :start => true, :stop => true
  action [ :enable, :start]
end

bash "bugzilla_install" do
  user "root"
  code <<-EOH
    cd /var/www
    rm -rm html
    wget https://ftp.mozilla.org/pub/mozilla.org/webtools/bugzilla-5.0.2.tar.gz
    tar -xvf bugzilla-5.0.2.tar.gz
    mv bugzilla-5.0.2/ html/
    cd /var/www/html 
    sudo ./checksetup.pl --check-modules
    sudo perl install-module.pl --all
    a2ensite bugzilla
    a2enmod cgi headers expires
    service apache2 restart

    touch /var/chef/cache/bugzilla.lock
  EOH
  action :run
  not_if {File.exists?("/var/chef/cache/bugzilla.lock")}
end


# mysql -u root -p -e "GRANT ALL PRIVILEGES ON bugs.* TO bugs@localhost IDENTIFIED BY '$db_pass'"

template "/etc/apache2/sites-available/bugzilla.conf" do
    path "/etc/apache2/sites-available/bugzilla.conf"
    source "bugzilla.conf.erb"
    owner "root"
    group "root"
    mode "0755"
    notifies :restart, resources(:service => "apache2")
end

template "/var/www/html/localconfig" do
    path "/var/www/html/localconfig"
    source "localconfig.erb"
    owner "root"
    group "root"
    mode "0755"
    notifies :restart, resources(:service => "apache2")
end






